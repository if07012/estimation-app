import * as React from 'react';
import { Route, Router } from 'react-router-dom';
import history from './history';

const LoginPage = React.lazy(() => import("./components/LoginPageComponent"));
const ProjectManager = React.lazy(() => import("./components/ProjectManagerComponent"));
const Developer = React.lazy(() => import("./components/DeveloperComponent"));
export const routes = <Router history={history}>
   <div>
      <Route exact={true} path='/' component={LoginPage} />
      <Route path='/ProjectManager' component={ProjectManager} />
      <Route path='/Developer' component={Developer} />
   </div>
</Router>;

