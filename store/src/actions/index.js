import * as types from '../constants/ActionTypes'
import axios from 'axios'
import history from '../history';
const receiveProducts = users => ({
   type: types.RECEIVE_DATA,
   users
})
const loadingData = value => ({
   type: types.LOADING_DATA,
   isLoading: value
})

export const updateEstimation = (value) => dispatch => {
   dispatch({
      type: types.UPDATE_DATA,
      value
   });

}
const raiseMessage = value => ({
   type: types.DATA_NOT_FOUND,
   value: value
})
export const getAllUser = () => dispatch => {
   dispatch(loadingData(true));
   axios.get('http://localhost:3005/users').then(n => {
      dispatch(receiveProducts(n.data));
      dispatch(loadingData(false));
   })
}

export const saveEstimation = (value) => dispatch => {
   dispatch(loadingData(true));
   axios.post('http://localhost:3005/estimaton', value).then(n => {
      dispatch(loadingData(false));
   })
}

export const ShowEstimate = () => dispatch => {
   dispatch({ type: types.SHOW_ESTIMATE });
}

export const DoneEstimate = () => dispatch => {
   dispatch({ type: types.DONE_ESTIMATE });
}

export const resetAllStateUser = () => dispatch => {
   dispatch({ type: types.RESET });
}
export const authentication = (value) => dispatch => {
   dispatch(loadingData(true));
   if (value.length === 0) {
      return;
   }
   axios.get('http://localhost:3005/users/' + value).then(({ data }) => {
      if (data === 'Not Found') {
         dispatch(raiseMessage(data));
      } else {
         window.localStorage.setItem('currentUser', JSON.stringify(data))
         if (data.tag !== 'Project manager')
            history.push('/Developer')
         else
            history.push('/ProjectManager')
      }
      dispatch(loadingData(false));
   })
}

