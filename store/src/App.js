import React, { Component, Suspense } from "react";
import "./App.css";
import { Provider } from 'react-redux'
import { hot } from "react-hot-loader";
import { socket } from './socket'
import { createStore, applyMiddleware } from 'redux'

import reducer from './reducers'
import { BrowserRouter, Router } from 'react-router-dom';
import { routes } from './Routes'
import { getAllUser } from './actions'
import thunk from 'redux-thunk'
const middleware = [thunk]

const store = createStore(
  reducer,
  applyMiddleware(...middleware)
)

class App extends Component {
  state = {
    currentPage: ''
  }
  componentDidMount() {
    socket.on('connection', timestamp => {
    });
    store.dispatch(getAllUser())

  }
  onSubmit = () => {
  }
  render() {
    const baseUrl = '/';
    return (
      <Provider store={store}>
        <div className="App" onClick={this.onSubmit}>
          <Suspense fallback={<div>Loading...</div>}>
            <BrowserRouter children={routes} basename={baseUrl} />
          </Suspense>
        </div>
      </Provider>
    );
  }
}

export default hot(module)(App);
