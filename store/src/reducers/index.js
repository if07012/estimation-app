import { combineReducers } from 'redux'
import users from './users'

export default combineReducers({
   users
})

export const getUsers = state =>
   state.users.users.filter(n => n.tag !== 'Project manager');

export const getGroups = state => {
   const group = state.users.users.reduce((result, current) => {
      if (!result['result']) result['result'] = [];
      if (result['result'].filter((n) => n.group === current.group).length === 0) {
         const estimation = !current.estimation ? "" : current.estimation.toString();
         result['result'].push({ group: current.group, estimation: estimation });
      }
      else {
         const item = result['result'].filter((n) => n.group === current.group)[0];
         var index = result['result'].indexOf(item);
         if (index !== -1) {
            if (result['result'][index].estimation.indexOf(current.estimation) === -1) {
               if (current.estimation !== null && current.estimation !== undefined)
                  result['result'][index].estimation += " " + current.estimation;
            }
         }
      }
      return result;
   }, {})
   return group.result;
}