import {
   FETCH_DATA, RECEIVE_DATA, LOADING_DATA, DATA_NOT_FOUND, UPDATE_DATA, SHOW_ESTIMATE, DONE_ESTIMATE, RESET
} from '../constants/ActionTypes'
const initialState = {
   data: 2,
   users: [],
   isLoading: false,
   message: '',
   isEstimate: false,
   isReset: true
}
const cart = (state = initialState, action) => {
   switch (action.type) {
      case FETCH_DATA:
         return state;
      case RESET:
         const newUsers = state.users.map(n => {
            n.isConfirm = false;
            n.estimation = undefined;
            return n;
         })

         return { ...state, users: newUsers }
      case DATA_NOT_FOUND:
         return { ...state, message: action.message };
      case LOADING_DATA:
         console.log(action.isLoading)
         return { ...state, isLoading: action.isLoading };
      case SHOW_ESTIMATE:
         debugger;
         return { ...state, isEstimate: true }
      case DONE_ESTIMATE:
         return { ...state, isEstimate: false, isReset: true }
      case RECEIVE_DATA:
         return { ...state, users: action.users }
      case UPDATE_DATA:
         const users = state.users.map(n => {
            if (n.id === action.value.id) {
               n.isConfirm = true;
               n.estimation = action.value.value;
            }
            return n;
         })

         return { ...state, users: users }
      default:
         return state
   }
}

export default cart
