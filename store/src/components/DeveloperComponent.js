import React, { Component } from 'react'
import { Card, Grid, Button } from 'semantic-ui-react'
import { CardComponent } from './CardComponent'
import { connect } from 'react-redux'
import { saveEstimation, DoneEstimate, ShowEstimate } from '../actions'
import { socket } from '../socket';
export class DeveloperComponent extends Component {
   state = {
      currentUrl: "",
      currentState: "",
      estimations: []
   }
   componentDidMount() {
      this.setState({ currentState: 'preview', estimations: [0.5, 1, 2, 4, 6, 8, 12, 16, '~'] })
      const id = JSON.parse(window.localStorage.getItem('currentUser')).id;
      console.log(id + " connected to server")
      const self = this;
      socket.emit('subscribe', id);
      socket.on(id, msg => {
         if (msg.key === 'ChangeUrl') {
            console.log("Change url to : " + msg.value)
            self.setState({ currentUrl: msg.value })
            self.props.DoneEstimate();
            self.onBack();
         } else if (msg.key === 'ShowEstimate') {
            console.log("Show Estimate");
            self.props.ShowEstimate();
         }
         else if (msg.key === 'DoneEstimate') {
            console.log("Done Estimate");
            self.props.DoneEstimate();
         }
      });
   }
   renderEstimation() {
      return <React.Fragment>
         <Grid divided='vertically'>
            <Grid.Row >
               <Grid.Column width={12}>
               </Grid.Column>
               <Grid.Column width={4}>
                  <div className='ui two buttons'>
                     <Button basic color='blue' onClick={this.onBack} icon='arrow left' content='Back' />
                  </div>
               </Grid.Column>
            </Grid.Row>
            <Grid.Row>
               <Grid.Column width={16}>
                  <Card.Group centered>
                     {this.state.estimations.map((item, index) => {
                        return <CardComponent key={index} onClick={this.onClick} item={item} />
                     })}
                  </Card.Group>
               </Grid.Column>
            </Grid.Row>
         </Grid>
      </React.Fragment>
   }
   onEstimate = () => {
      this.setState({ currentState: 'estimate' })
   }
   onCommit = () => {
      this.setState({ currentState: 'preview' })
   }
   onClick = (item) => {
      socket.emit('estimation', { value: item, id: '746a3039-5f74-4a62-8673-0aa9a3e06be9' })
   }
   onBack = () => {
      this.setState({ currentState: 'preview' })
   }
   renderPreview() {
      if (!this.props.isPreview) {
         return <React.Fragment>
            <iframe src={this.state.currentUrl} title="Key frame" width="100%" height="100%" />
         </React.Fragment>
      }
      return <React.Fragment>
         <div style={{ 'position': 'fixed', 'width': '100%', top: 0, left: '0', overflowY: 'hidden', overflowX: 'hidden' }}>
            <Grid divided='vertically'>
               <Grid.Row >
                  <Grid.Column width={12}>
                  </Grid.Column>
                  <Grid.Column width={4}>
                     <Card.Group centered itemsPerRow={1}>
                        <Card>
                           <Card.Content>
                              <div className='ui two buttons'>
                                 <Button basic color='green' onClick={this.onEstimate}>
                                    Estimate
                                 </Button>
                              </div>
                           </Card.Content>
                        </Card>
                     </Card.Group>
                  </Grid.Column>
               </Grid.Row>
            </Grid>
         </div>
         <iframe src={this.state.currentUrl} title="Key frame" width="100%" height="100%" />
      </React.Fragment>
   }
   render() {
      return (
         <div style={{ 'position': 'fixed', 'height': '100%', 'width': '100%', top: 0, left: '0', overflowY: 'hidden', overflowX: 'hidden' }}>
            {this.state.currentState === 'preview' ? this.renderPreview() : this.renderEstimation()}
         </div>
      )
   }
}

const mapStateToProps = (state) => ({
   isPreview: state.users.isEstimate
})

export default connect(
   mapStateToProps, {
      saveEstimation, ShowEstimate,
      DoneEstimate
   }
)(DeveloperComponent)

