import React, { Component } from 'react'
import { Button, Card, Image } from 'semantic-ui-react'
import ItemContentComponent from './ItemContentComponent';
export class ContentComponent extends Component {
   render() {
      return (
         <Card.Group centered>
            {this.props.data.map((item, index) => {
               return <ItemContentComponent key={index} changeQuantity={this.props.changeQuantity} data={item} addToChart={this.props.addToChart} />
            })}
         </Card.Group>
      )
   }
}

export default ContentComponent
