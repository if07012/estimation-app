import React, { Component } from 'react'
import { Image, List } from 'semantic-ui-react'
import CheckouItemComponent from './CheckouItemComponent';
export class CheckoutComponent extends Component {

   render() {
      return (
         <List celled>
            {this.props.data.map((item, index) => {
               return <CheckouItemComponent item={item} key={index} handleChange={this.props.handleChange} delete={this.props.delete} />
            })}

         </List>
      )
   }
}

export default CheckoutComponent
