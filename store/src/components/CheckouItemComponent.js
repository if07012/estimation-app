import React, { Component } from 'react'
import { Image, List, Grid, Button, Input } from 'semantic-ui-react'
export class CheckouItemComponent extends Component {
   state = {
      value: 1
   }
   handleChange = (e) => {
      this.setState({ value: e.target.value })
   }
   render() {
      return (
         <List.Item>
            <Image size='small' src={this.props.item.image} />
            <List.Content>
               <List.Header>{this.props.item.name}</List.Header>
               {this.props.item.category}
               <Grid divided='vertically'>
                  <Grid.Row >
                     <Grid.Column width={3}>
                        <Button icon='plus' onClick={(e) => this.props.handleChange(this.props.item, this.props.item.value + 1)} />
                     </Grid.Column>
                     <Grid.Column width={9}>
                        <Input style={{ 'width': '100%' }} type="number"
                           placeholder='Quantity...' value={this.props.item.value}
                           onChange={(e) => this.props.handleChange(this.props.item, e.target.value)} />
                     </Grid.Column>
                     <Grid.Column width={3}>
                        <Button icon='minus' onClick={(e) => this.props.handleChange(this.props.item, this.props.item.value - 1)} />
                     </Grid.Column>
                  </Grid.Row>
               </Grid>
               <div className='ui two buttons'>
                  <Button basic color='green' onClick={() => this.props.delete(this.props.item)}>
                     Delete
                  </Button>
               </div>
            </List.Content>
         </List.Item>
      )
   }
}

export default CheckouItemComponent
