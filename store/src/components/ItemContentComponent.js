import React, { Component } from 'react'
import { Button, Card, Image, Grid, Divider, Input } from 'semantic-ui-react'
export class ItemContentComponent extends Component {
   state = {
      value: 1
   }
   handleChange = (e) => {
      this.setState({ value: e.target.value })
   }
   render() {
      return (
         <Card>
            <Card.Content>
               <Image floated='right' size='large' src={this.props.data.image} />
               <Card.Header>{this.props.data.name}</Card.Header>
               <Card.Meta>{this.props.data.price}</Card.Meta>
               <Card.Description>
                  <strong>{this.props.data.category}</strong>
               </Card.Description>
            </Card.Content>
            <Card.Content extra>
               <Grid divided='vertically'>
                  <Grid.Row >
                     <Grid.Column width={3}>
                        <Button icon='plus' onClick={(e) => {
                           this.props.changeQuantity(this.props.data, this.props.data.value += 1)
                        }} />
                     </Grid.Column>
                     <Grid.Column width={9}>
                        <Input style={{ 'width': '100%' }} placeholder='Quantity...' value={this.props.data.value} onChange={this.handleChange} />
                     </Grid.Column>
                     <Grid.Column width={3}>
                        <Button icon='minus' onClick={(e) => {
                           this.props.changeQuantity(this.props.data, this.props.data.value -= 1)
                        }} />
                     </Grid.Column>
                  </Grid.Row>
               </Grid>
               <Divider />
               <div className='ui two buttons'>
                  <Button basic color='green' onClick={() => this.props.addToChart(this.props.data, this.props.data.value)}>
                     Add To Chart
                  </Button>
               </div>
            </Card.Content>
         </Card>
      )
   }
}

export default ItemContentComponent
