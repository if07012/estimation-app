import React, { Component } from 'react'
import { Input, Menu } from 'semantic-ui-react'
export class SearchComponent extends Component {
   render() {
      return (
         <Menu.Item>
            <Input className='icon' value={this.props.keyword} onChange={this.props.handleChange} icon='search' placeholder='Search...' />
         </Menu.Item>
      )
   }
}

export default SearchComponent
