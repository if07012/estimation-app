import React, { Component } from 'react'
import { Card, Segment } from 'semantic-ui-react'
import { Button, Grid, Container, Form, Input } from 'semantic-ui-react'
import { connect } from 'react-redux'
import { authentication } from '../actions'
export class LoginPageComponent extends Component {
   state = {
      token: ""
   };
   handleOnChange = (e) => {
      this.setState({ token: e.target.value });
   }
   onValidate = (token) => {
      this.setState({ currentPage: 'ProjectManager' })
      this.props.authentication(this.state.token);
   }
   render() {
      return (
         <Container textAlign='center'>Center Aligned
         <Form className="middle">
               <Card.Group centered>
                  <Card>
                     <Card.Header><h1>Login To System</h1></Card.Header>
                     <Card.Meta>Please input your token</Card.Meta>
                     <Card.Description>
                        <Grid>
                           <Grid.Row >
                              <Grid.Column>
                                 <Input style={{ 'width': '100%' }} placeholder='' value={this.state.token} onChange={this.handleOnChange} />
                              </Grid.Column>
                           </Grid.Row>
                        </Grid>
                        <div className='ui two buttons'>
                           <Button basic color='green' onClick={(e) => this.onValidate(this.state.token)}>
                              Validate
                        </Button>
                        </div>
                     </Card.Description>
                  </Card>
               </Card.Group>
            </Form >
         </Container>
      )
   }
}

const mapStateToProps = (state) => ({

})

export default connect(
   mapStateToProps, {
      authentication
   }
)(LoginPageComponent)
