import React, { Component } from 'react'
import logo from "../logo.svg";
import { Menu } from 'semantic-ui-react'
function getSum(total, num) {
   return total + num;
}
export class MenuComponent extends Component {
   render() {
      let info = 0;
      if (this.props.data && this.props.data.length > 0)
         this.props.data.map(n => info += (n.value * n.price));
      return (
         <React.Fragment>
            <Menu.Menu position='right'>
               <Menu.Item
                  name={info}
                  icon="dollar"
                  onClick={this.props.checkout}
               />
               <Menu.Item>
                  <img src={logo} className="App-logo" alt="logo" />
               </Menu.Item>
            </Menu.Menu>
         </React.Fragment>
      )
   }
}

export default MenuComponent
