import React, { Component } from 'react'
import { Card, Image, Icon } from 'semantic-ui-react'
import { socket } from '../socket'
export class UserInformationComponent extends Component {
   renderLoading() {
      if (this.props.view === 'view')
         return <span>Estimation is <strong>{this.props.item.estimation}</strong> hours</span>
      if (this.props.item.isConfirm)
         return <div>
            <Icon color="green" size='big' name='check square' /> <strong>Confirm</strong>
         </div>
      if (this.props.view === 'estimate')
         return <div>
            <img src='/images/loading.gif' alt='Loading...' />
         </div>

      return <div></div>
   }
   componentDidMount() {
      socket.emit('subscribe', this.props.item.id)
   }
   render() {
      return (
         <Card>
            <Card.Content>
               <Image floated='right' size='mini' src='/images/user.png' />
               <Card.Header>{this.props.item.name}</Card.Header>
               <Card.Meta>{this.props.item.tag}</Card.Meta>
               <Card.Description>
                  {this.renderLoading()}

               </Card.Description>
            </Card.Content>
         </Card>
      )
   }
}

export default UserInformationComponent
