import React, { Component } from 'react'
import { Card, Grid, Button, Label, Input } from 'semantic-ui-react'
import UserInformationComponent from './UserInformationComponent';
import { connect } from 'react-redux'
import { getAllUser, updateEstimation, resetAllStateUser } from '../actions'
import { socket } from '../socket'
import { getUsers, getGroups } from '../reducers'
import axios from 'axios'
export class ProjectManagerComponent extends Component {
   state = {
      listUser: [],
      isViewing: false,
      estimatedByGroup: [],
      currentState: "",
      isLoading: false,
      currentUrl: "",
      value: ""
   }

   handleChange = (e) => {
      this.setState({ value: e.target.value })
   }
   componentDidMount() {
      this.setState({ currentUrl: "http://localhost/" })
      socket.on('estimation', e => {
         this.props.updateEstimation(e);
      });
   }

   OnViewHandle = (self) => {
      self.setState({ currentState: "view" })
   }
   OnCommitHandle = () => {
      this.setState({ isLoading: true })
      const self = this;
      setTimeout(() => {
         self.props.resetAllStateUser();
         this.setState({ currentState: "", isLoading: false });
         socket.emit('ChangeUrl', 'http://localhost')
      }, 2000);

   }
   OnChangeUrl = () => {
      socket.emit('ChangeUrl', this.state.value)
      console.log("Change Url to " + this.state.value)
   }
   OnBackHandle = () => {
      this.setState({ currentState: "", isLoading: false })
   }
   renderGroup = () => {
      const colors = ['blue', 'green', 'red'];
      if (this.state.currentState === 'view') {
         return <React.Fragment>
            {this.props.groups.map((item, index) => {
               return <Label basic color={colors[index]} key={index}>
                  {item.group} : {item.estimation}
               </Label>
            })}
         </React.Fragment>
      }
      return <div></div>
   }
   OnEstimationHandle = () => {
      let listUser = this.state.listUser;
      for (const key in listUser) {
         if (listUser.hasOwnProperty(key)) {
            const element = listUser[key];
            element.isConfirm = false;
         }
      }
      this.setState({ currentState: 'estimate', listUser: listUser })
      socket.emit('ShowEstimate', this.state.value)
   }
   renderButtonView() {
      return <React.Fragment>
         <div>
            <Input style={{ 'width': '100%' }} placeholder='Please wait url' value={this.state.value} onChange={this.handleChange} />
         </div>
         <div className='ui two buttons'>
            <Button basic color='green' content='Change Url' icon='magnify' disabled={this.state.currentState === 'view'} onClick={() => { this.OnChangeUrl(this) }} />
         </div>
         <div className='ui two buttons'>
            <Button basic color='green' content='View' icon='magnify' disabled={this.state.currentState === 'view'} onClick={() => { this.OnViewHandle(this) }} />
            <Button basic color='blue' content='Estimation' icon='file' onClick={() => { this.OnEstimationHandle(this) }} />
         </div>
      </React.Fragment>
   }
   render() {
      return (
         <div>
            <Grid divided='vertically'>
               <Grid.Row >
                  <Grid.Column width={4} style={{ paddingLeft: '1em' }}>
                     <Card.Group centered itemsPerRow={1}>
                        <Card style={{ boxShadow: 'none' }}>
                           <Card.Content>
                              {this.state.currentState === 'view' ? "" : this.renderButtonView()}
                              <div className='ui two buttons'>
                                 {this.state.currentState !== 'view' ? "" : <React.Fragment> <Button basic color='red' content='Back' icon='arrow left' onClick={this.OnBackHandle} />
                                    <Button basic color='green' content='Commit' icon='save' loading={this.state.isLoading} onClick={this.OnCommitHandle} /></React.Fragment>}

                              </div>
                           </Card.Content>
                           <Card.Content extra>
                              {this.renderGroup()}
                           </Card.Content>
                        </Card>
                        {this.props.isLoading && <div>Loading...</div>}
                        {this.props.users.map((item, index) => {
                           return <UserInformationComponent key={index} item={item} view={this.state.currentState} />
                        })}
                     </Card.Group>
                  </Grid.Column>
                  <Grid.Column width={12}>
                     <iframe src={this.state.currentUrl} title="Key frame" width="100%" height="100%" />
                  </Grid.Column>
               </Grid.Row>
            </Grid>

         </div>
      )
   }
}

const mapStateToProps = (state) => ({
   users: getUsers(state),
   groups: getGroups(state),
   isLoading: state.users.isLoading
})

export default connect(
   mapStateToProps, {
      getAllUser,
      updateEstimation,
      resetAllStateUser
   }
)(ProjectManagerComponent)
