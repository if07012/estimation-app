import React, { Component } from 'react'
import { Button, Card, Image, Grid, Divider, Input } from 'semantic-ui-react'
import { connect } from 'react-redux'
import { saveEstimation } from '../actions'
export class CardComponent extends Component {

   render() {
      return (
         <Card onClick={() => { this.props.onClick(this.props.item) }}>
            <Card.Content>
               <h1 style={{ fontSize: '170px', textAlign: 'center' }}>{this.props.item}</h1>
            </Card.Content>
         </Card>
      )
   }
}


const mapStateToProps = (state) => ({

})

export default connect(
   mapStateToProps, {
      saveEstimation
   }
)(CardComponent)

