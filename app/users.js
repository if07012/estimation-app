const users = [
   {
      "name": "Brice",
      "tag": "Project manager",
      "group": "SD",
      "isConfirm": false,
      "id": "60945e07-e8ee-45b2-81e1-1c6eab4e1b49"
   },
   {
      "name": "Andy Santosa",
      "tag": "Software Developer",
      "group": "SD",
      "isConfirm": false,
      "id": "60945e07-e8ee-45b2-81e1-1c6eab4e3b49"
   },
   {
      "name": "Asep Ridwan",
      "tag": "Software Developer",
      "group": "SD",
      "estimation": null,
      "id": "746a3039-5f74-4a62-8673-0aa9a3e06be9"
   },
   {
      "name": "Harizal Hilmi",
      "tag": "Software Developer",
      "group": "SD",
      "estimation": null, "id": "bc0ad8fc-b469-4ef8-b59c-7b9699c84c1c"
   },
   {
      "name": "Deky",
      "tag": "Software Developer",
      "group": "SD",
      "estimation": null,
      "id": "060a4ed7-9da4-4ed4-ab7d-5c70633cd187"
   },
   {
      "name": "Bambang",
      "tag": "Front End Developer",
      "group": "FE",
      "estimation": null,
      "id": "007bedea-d09c-4b54-9560-39cce3b9d673"
   },
   {
      "name": "Bogi",
      "tag": "System Tester",
      "group": "QA",
      "estimation": null,
      "id": "c7f7f703-fe1a-4ca8-ab16-157366e63a05"
   }
];

module.exports = users;