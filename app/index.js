const users = require('./users');
const express = require('express')

const app = express()
app.use(function (req, res, next) {
   res.header("Access-Control-Allow-Origin", "*");
   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
   next();
});
const http = require('http').Server(app);
var io = require('socket.io')(http);
const port = 3005

app.get('/users', (req, res) => {
   res.send(users);
});
app.get('/users/:id', (req, res) => {
   const temps = users.filter(n => n.id === req.params.id);
   if (temps.length > 0)
      res.send(temps[0]);
   else res.send('Not Found')
})
io.on('connection', function (socket) {
   console.log('a user connected');
   socket.on('subscribe', (n) => {
      console.log('Subscribe : ' + n)
      socket.on('ChangeUrl', url => {
         console.log('sent to ChangeUrl ' + n)
         io.emit(n, { key: 'ChangeUrl', value: url })
      })
      socket.on('ShowEstimate', url => {
         console.log('sent to ShowEstimate ' + n)
         io.emit(n, { key: 'ShowEstimate', value: url })
      })
      socket.on('DoneEstimate', url => {
         console.log('sent to DoneEstimate ' + n)
         io.emit(n, { key: 'DoneEstimate', value: url })
      })
   });
   socket.on('estimation', (n) => {
      console.warn(n)
      io.emit('estimation', n)
   });
});

http.listen(port, () => console.log(`Example app listening2on port ${port}!`))